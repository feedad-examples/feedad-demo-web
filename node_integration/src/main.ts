import {AdResponse, loadFeedAd, Sdk} from "feedad-node";

const clientToken = "your-web-client-token";
const placementId = "your-placement-id";

async function main() {
	const feedad = await loadFeedAd();

	// Initialize the SDK
	await feedad.init(clientToken);
	await requestAds(feedad);
}

async function requestAds(feedad: Sdk) {
	// Request an ad
	let adResponse: AdResponse;
	try {
		adResponse = await feedad.requestAd(placementId);
	} catch (error) {
		console.log("Error requesting ad:", error);
		return;
	}

	// add the ad placements
	const adContainers: HTMLElement[] = [];
	const paragraphs = document.querySelectorAll("main > p");
	for (let i = 1; i < paragraphs.length; i += 3) {
		const paragraph = paragraphs.item(i);

		// the ad element
		const adElement = adResponse.createAdContainer();

		// the "ad" class limits the ad size (see index.html). Such styles will have to be applied using a container element.
		const adContainer = document.createElement("div");
		adContainer.classList.add("ad");
		adContainer.appendChild(adElement)
		paragraph.insertAdjacentElement("afterend", adContainer);
		adContainers.push(adContainer);
	}

	// wait for the ad to finish and remove the ad elements when playback completed
	try {
		await adResponse.promise;
	} catch (error) {
		console.log("error during playback", error);
	} finally {
		adContainers.forEach(adContainer => adContainer.parentNode?.removeChild(adContainer));
	}
}

main().catch(error => {
	console.warn("error during ads request: ", error);
});