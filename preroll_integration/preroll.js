document.addEventListener("DOMContentLoaded", function () {

	// SDK Config: replace this:
	const webClientToken = "your-web-client-token";
	const placementId = "your-placement-id";

	const player = document.getElementById("player");
	const btnPlay = document.getElementById("btn-play");
	const statusDisplay = document.getElementById("status");
	const adContainer = document.getElementById("ad-container");

	function status(what) {
		statusDisplay.innerText = what;
	}

	function startContentVideo() {
		btnPlay.style.display = "none";
		adContainer.parentNode.removeChild(adContainer);
		player.controls = true;
		player.loop = true;
		status("loading content");
		player.play()
			  .then(function () {
				  status("content playing");
			  });
	}

	status("waiting for FeedAd SDK");

	// initialize the SDK
	feedad.cmd.push(() => {
		status("initializing FeedAd SDK");
		feedad.sdk.init(webClientToken);
	});

	feedad.cmd.push(() => {
		if (!feedad.sdk.isSupported()) {
			status("FeedAd SDK is not supported by this browser");
			btnPlay.onclick = startContentVideo;
			return;
		}

		// setup pre-roll ad
		btnPlay.onclick = () => {
			status("loading ad");

			// Request the pre-roll ad:
			// use "requestStandaloneAd" ads for pre-rolls, because "requestAd" might serve infinite ads.
			feedad.sdk.requestStandaloneAd(placementId)
				  .then(adResponse => {
					  console.log("ad response received");

					  // the ad element
					  const adElement = adResponse.createAdContainer();

					  //insert the ad element
					  adContainer.appendChild(adElement);
					  adContainer.classList.add("active");

					  status("ad playing");
					  return adResponse.promise;
				  })
				  .then(() => {
					  console.log("ad playback completed");
					  startContentVideo();
				  })
				  .catch(e => {
					  // if the ad request went wrong the reason will be logged.
					  console.log("error requesting ad", e);
					  startContentVideo();
				  });
		};
		status("waiting for play button interaction");
	});

}, false);