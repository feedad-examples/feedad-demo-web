# FeedAd Web SDK Demo

This project shows several ways to integrate the FeedAd Web SDK into your page.
For information about FeedAds in general and detailed documentation of the Web SDK, you should check out our [documentation](https://docs.feedad.com).

## Project examples

Each example has its own subdirectory named after the integration type.

### AMP

Shows how to include a FeedAd within an AMP page.

Read more about the AMP integration on the [AMP documentation](https://github.com/ampproject/amphtml/blob/master/ads/feedad.md) or the [FeedAd documentation](https://docs.feedad.com/web/installation/amp/).

### Node Integration

Shows how to include the SDK within a Node.JS project.

This example uses Type-Script and Vite, but our FeedAd Project can be used with any bundler that is compatible with UMD packages.

To build the example:

1. `cd ./node_integration`
2. `npm i`
3. `npm run dev`

### Prebid

Shows how to set up FeedAd as a Prebid.JS bidder.

### Pre-Roll Integration

Shows how the FeedAd SDK can be used to display Pre-Roll advertisements before video ads.

### Script Integration

This example shows how to include the FeedAd SDK into a website using only `<script>` tags.

## Running the Example Project

Some examples might not work if you just open the index files in your browser. 
You will have to start a local web server at the root of the project.
The example project does not include a method to do so, because most IDEs come with a built-in solution for static development web servers.

If you have node installed on your machine, just run `npx http-server --cors` in the root directory of your project, to start a local web server.