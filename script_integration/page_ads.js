// optional: set error callback that is called when the SDK cannot be loaded
feedad.onError = function (error) {
	console.warn("The FeedAd SDK could not be loaded:", error);
};

// initialize the SDK
feedad.cmd.push(function () {
	// Initialize the SDK
	feedad.sdk.init("your-web-client-token");
});

// request a FeedAd
feedad.cmd.push(function () {
	feedad.sdk.requestAd("your-placement-id")
		  .then(function (adResponse) {
			  // Received an ad response.
			  console.log("ad response received");

			  // add the ad placements
			  const adContainers = [];
			  const paragraphs = document.querySelectorAll("main > p");
			  for (let i = 1; i < paragraphs.length; i += 3) {
				  const paragraph = paragraphs.item(i);

				  // the ad element
				  const adElement = adResponse.createAdContainer();

				  // the "ad" class limits the ad size (see index.html). Such styles will have to be applied using a container element.
				  const adContainer = document.createElement("div");
				  adContainer.classList.add("ad");
				  adContainer.appendChild(adElement)
				  paragraph.insertAdjacentElement("afterend", adContainer);
				  adContainers.push(adContainer);
			  }

			  const cleanup = () => {
				  adContainers.forEach(adContainer => adContainer.parentNode.removeChild(adContainer));
			  }

			  // wait for the ad to finish and remove the ad elements when playback completed
			  return adResponse.promise
							   .then(function () {
								   console.log("ad playback completed");
								   cleanup();
							   })
							   .catch(function (e) {
								   console.log("error during playback", e);
								   cleanup();
							   });
		  })
		  .catch(function (e) {
			  // if the ad request went wrong the reason will be logged.
			  console.log("error requesting ad", e);
		  });
});